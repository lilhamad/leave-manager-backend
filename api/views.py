from rest_framework import generics
from .serializers import UserSerializer, LeaveSerializer
from employee.models import Employee
from leave.models import Leave
from rest_framework import permissions
from .permissions import IsOwnerOrReadOnly
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from django.http import HttpResponse, JsonResponse
from rest_framework.authtoken.models import Token
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, permission_classes
from django.db.models import Q
from django.contrib.auth import get_user_model

class UserList(generics.ListCreateAPIView):
    authentication_classes = () 
    queryset = Employee.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    queryset = Employee.objects.all()
    serializer_class = UserSerializer

class LeaveList(generics.ListCreateAPIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]
    queryset = Leave.objects.all()
    serializer_class = LeaveSerializer


class LeaveDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    queryset = Leave.objects.all()
    serializer_class = LeaveSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    permission_classes = []
    authentication_classes = () 
    queryset = get_user_model().objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


@csrf_exempt
@api_view(["POST"])
def login(request):
    permission_classes = []
    authentication_classes = () 
    print('here to login')
    username = request.data.get("email")
    password = request.data.get("password")
    if username is None or password is None:
        return Response({'error': 'Please provide both email and password'},
                        status=status.HTTP_400_BAD_REQUEST)
    try:
        user = get_user_model().objects.get(Q(email__iexact=username))
    except get_user_model().DoesNotExist:
        return Response({'error': 'Invalid Credentials'},
                        status=status.HTTP_404_NOT_FOUND)
    token, _ = Token.objects.get_or_create(user=user)
    data = {}
    data["first_name"]=user.first_name
    data["last_name"]=user.last_name
    data["email"]=user.email
    return JsonResponse({'token': token.key, 'is_admin':user.is_admin,'first_name':user.first_name, 'last_name':user.last_name, 'email':user.email, 'id':user.id})

