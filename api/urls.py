from django.urls import include, path
from rest_framework import routers
from . import views
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.urlpatterns import format_suffix_patterns

# router = routers.DefaultRouter()
# router.register(r'users', views.UserViewSet)
# router.register(r'leaves', views.LeaveViewSet)

urlpatterns = [
    # path('', include(router.urls)),
    path('auth/', ObtainAuthToken.as_view()),
    path('api-auth/', include('rest_framework.urls')),
    path('login/', views.login),
    path('users/', views.UserList.as_view()),
    path('snippets/<int:pk>/', views.UserDetail.as_view()),
    path('leaves/', views.LeaveList.as_view()),
    path('leaves/<int:pk>/', views.LeaveDetail.as_view()),

]
urlpatterns = format_suffix_patterns(urlpatterns)
